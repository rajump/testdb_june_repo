﻿-- <Migration ID="bfd4a70b-f3ed-410e-98f2-b1305edf2b4c" />
GO

PRINT N'Creating [dbo].[city]'
GO
CREATE TABLE [dbo].[city]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[city_name] [char] (128) NOT NULL,
[lat] [decimal] (9, 6) NOT NULL,
[long] [decimal] (9, 6) NOT NULL,
[country_id] [int] NOT NULL
)
GO
